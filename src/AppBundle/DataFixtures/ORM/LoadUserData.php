<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 1;
    }

    public function load(ObjectManager $manager)
    {
        $user1 = new User;
        $user1->setName('DPAVIC85');
        $user1->setEmail('drazen.pavic@gmail.com');
        
        $user2 = new User;
        $user2->setName('test');
        $user2->setEmail('test@test.dev');
        
        $manager->persist($user1);
        $manager->persist($user2);
        
        $manager->flush();
        
        $this->addReference('dpavic', $user1);
        $this->addReference('test-user', $user2);
    }

}
