<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Tweet;

class LoadTweetData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 2;
    }

    public function load(ObjectManager $manager)
    {
        $tweet1 = new Tweet();
        $tweet1->setBody('This is my First Tweet #FirstTweet #testing #test');
        $tweet1->setPublishAt(new \DateTime());
        $tweet1->setStatus();
        $tweet1->setUser($this->getReference('dpavic'));
        
        $tweet2 = new Tweet();
        $tweet2->setBody('This is my Second Tweet #SecondTweet #testing #test');
        $tweet2->setPublishAt(new \DateTime('+2 days'));
        $tweet2->setStatus();
        $tweet2->setUser($this->getReference('dpavic'));
        
        $tweet3 = new Tweet();
        $tweet3->setBody('TestUser First Tweet #TestUser #foo #bar #baz');
        $tweet3->setPublishAt(new \DateTime('+3 days'));
        $tweet3->setStatus();
        $tweet3->setUser($this->getReference('test-user'));
        
        $manager->persist($tweet1);
        $manager->persist($tweet2);
        $manager->persist($tweet3);
        
        $manager->flush();
    }

}
